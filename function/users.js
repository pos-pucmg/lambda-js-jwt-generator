const _ = require('lodash');

const RolesDB = [
  {
    role: 'SIGO_NORMAS_MANAGER',
    scopes: ['/sigo/mgn/*'], // Authorized actions
  },
  {
    role: 'SIGO_NORMAS_USER',
    scopes: ['sigo/mgn/download/norma/*','sigo/mgn/normas*'], // Authorized actions
  },
  {
    role: 'SIGO_GESTAO_INDUSTRIAL_USER',
    scopes: ['/sigo/mgp/*'], // Authorized actions
  }
];

const UsersDB = [
  {
    username: 'sigo-gestor-normas',
    password: 'sigo123', // User password
    roles: ['SIGO_NORMAS_MANAGER'], 
    scopes: []
  },
  {
    username: 'sigo-consultor-normas',
    password: 'sigo123',
    roles: ['SIGO_NORMAS_USER'],
    scopes: []
  },
  {
    username: 'sigo-consultor-processos',
    password: 'sigo123',
    roles: ['SIGO_GESTAO_INDUSTRIAL_USER'],
    scopes: []
  },
  {
    username: 'sigo-admin',
    password: 'sigo123',
    roles: ['SIGO_NORMAS_MANAGER','SIGO_GESTAO_INDUSTRIAL_USER'],
    scopes: []
  }
];

/**
  * Returns a user, given a username and valid password.
  *
  * @method login
  * @param {String} username - user id
  * @param {String} password  - Allow / Deny
  * @throws Will throw an error if a user is not found or if the password is wrong.
  * @returns {Object} user
  */
const login = (username, password) => {
  const user = _.find(UsersDB, { username });
  if (!user) throw new Error('Usuário não encontrado!');

  const hasValidPassword = (user.password === password);
  if (!hasValidPassword) throw new Error('Senha inválida!');
  
  user.roles.forEach(function getScopes(role) {
    user.scopes.push(_.find(RolesDB, { role }).scopes);
  });

  return _.omit(user, 'password');
};

module.exports = {
  login,
};
