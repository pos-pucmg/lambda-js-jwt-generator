const AWSXRay = require('aws-xray-sdk-core')
const AWS = AWSXRay.captureAWS(require('aws-sdk'))

// Create client outside of handler to reuse
const lambda = new AWS.Lambda()

const jwt = require('jsonwebtoken');
const users = require('./users');

const JWT_EXPIRATION_TIME = '1h';
const JWT_SECRET = 'sigo-secret'

/**
  * POST /sessions
  *
  * Returns a JWT, given a username and password.
  * @method login
  * @param {String} event.body.username
  * @param {String} event.body.password
  * @throws Returns 401 if the user is not found or password is invalid.
  * @returns {Object} jwt that expires in 1h
  */
exports.handler = (event, context, callback) => {
  // console.log('event');
  // console.log(event);
  // console.log('body');
  // console.log(event.body);
  // console.log(`body username = ${event.body.username}`);

  const { username, password } = JSON.parse(event.body);
  //console.log(`body json = ${JSON.stringify(JSON.parse(event.body))}`);

  try {
    // Authenticate user
    const user = users.login(username, password);
    console.log(user);

    // Issue JWT
    const token = jwt.sign({ user }, JWT_SECRET, { expiresIn: JWT_EXPIRATION_TIME });
    console.log(`JWT issued: ${token}`);
    const response = { // Success response
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        scopes: user.scopes,
        roles: user.roles,
        accessToken : token
      }),
    };

    // Return response
    console.log(response);
    callback(null, response);
  } catch (e) {
    console.log(`Error logging in: ${e.message}`);
    const response = { // Error response
      statusCode: 401,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      body: JSON.stringify({
        error: e.message,
      }),
    };
    
    callback(null, response);
  }
};